// 引入 express
const express = require('express');
// 建立 web server 物件
const app = express();
// 引入核心套件 url
const url = require('url');
// 引入套件 bodyParser
const bodyParser = require('body-parser')
// 引入套件 multer
const multer = require('multer');
// multer 設定上傳暫存目錄
const upload = multer({dest:'tmp_uploads/'});
// 處理檔案的核心套件
const fs = require('fs');

// 註冊樣版引擎
app.set('view engine', 'ejs');

//// MiddleWare
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
// 路由
app.get('/', function(request, response){ response.send('Hey!'); });


// 偵聽
app.listen(3000, function(){ console.log('啟動 server 偵聽埠號 3000'); });

